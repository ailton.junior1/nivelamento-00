function GoToCalculator() {
    window.location.assign('calculator.html')
}

function Calculate () {
    const elementResolved = document.getElementById("resolved");
    const radios = document.getElementsByClassName('operation');
}

let operation = "";

radios.forEach(radio=>{
    if(radio.check) {
        operation = radio.value
    }

})

const Factor1 = Number(document.getElementById('factor1').value);
const Factor2 = Number(document.getElementById('factor2').value);

let resolved = 0;

switch(operation){
    case 'multi':
        resolved = Number(Factor1 * Factor2);
        break;
    case 'division':
        resolved = Number(Factor1 / Factor2);
        break;
}
    
elementResolved.innerHTML = resolved