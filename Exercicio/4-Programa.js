//04 - Escreva um programa que apresente na tela do usuário uma lista de opções como apresentado abaixo:
//1 - Somar | 2 - Subtrair | 3 - Muiltiplicar | 4 - Dividir | -1 Sair 
//Após apresentar as opções ao usuário o programa deverá recolher do usuário a opção desejada:
// Se o valor informado for igual a uma operação matemática, o programa deverá solicitar o primeiro número ao usuário, na sequência deverá solicitar 
//o segundo número e por fim apresentar ao usuário o resultado final da operação. 
//Se o valor informado for -1 que corresponde a opção Sair apresentada o sistema deverá apresentar uma mensagem avisando que está desligando e o programa deve ser encerrado.


const readlineSync = require('readline-sync');

const informeApp = readlineSync.question('Informe a opcao desejada: 1 - Somar | 2 - Subtrair | 3 - Multiplicar | 4 - Dividir | -1 Sair ');

if (informeApp == 1) {
    var numero1 = Number(readlineSync.question('Digite o primeiro numero: '));
    var numero2 = Number(readlineSync.question('Digite o segundo numero:'));
    var resultado = Number(numero1 + numero2);
    console.log('O resultado de ', numero1, '+', numero2, 'e igual a ', resultado);
}
else if (informeApp == 2) {
    var numero1 = Number(readlineSync.question('Digite o primeiro numero: '));
    var numero2 = Number(readlineSync.question('Digite o segundo numero:'));
    var resultado = Number(numero1 - numero2);
    console.log('O resultado de ', numero1, '-', numero2, 'e igual a ', resultado);

}

else if (informeApp == 3) {
    var numero1 = Number(readlineSync.question('Digite o primeiro numero: '));
    var numero2 = Number(readlineSync.question('Digite o segundo numero:'));
    var resultado = Number(numero1 * numero2);
    console.log('O resultado de ', numero1, '*', numero2, 'e igual a ', resultado);

}

else if (informeApp == 4) {
    var numero1 = Number(readlineSync.question('Digite o primeiro numero: '));
    var numero2 = Number(readlineSync.question('Digite o segundo numero:'));
    var resultado = Number(numero1 / numero2);
    console.log('O resultado de ', numero1, '/', numero2, 'e igual a ', resultado);
}
else if (informeApp == -1) {
    console.log('O programa sera finalizado');
}
